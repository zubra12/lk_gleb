var Login = (new function() {

    var self = new Module("login");

    self.username = ko.observable("");
    self.password = ko.observable("");

    self.login = function() {
        self.rPost("login", { username: self.username(), password: self.password() }, function(data) {
            if (data.err) {
                self.error(data.err);
            } else {
                self.init(function() {
                    MSite.init(function() {
                        pager.navigate("/");
                        Bus.emit("user-inited");
                    })
                })
            }
        })
    }

    self.logOut = function() {
        self.rPost("logout", {}, function(data) {
            return window.location.reload();
            self.init(function() {
                MSite.init(function() {
                    pager.navigate("/");
                })
            })
        })
    }

    self.forceRedirectToAuth = function() {
        pager.navigate("/login");
    }

    self.init = function(done) {
        self.rGet("me", {}, function(data) {
            if (!data || !data.me) {
                MSite.Me(null);
                self.forceRedirectToAuth();
                return done();
            }
            MSite.Me(MModels.create("user", data.me));
            setTimeout(function(){
                Bus.emit("user-inited");
            },0);            
            return done();
        })
    }



    return self;
})

ModuleManager.Modules.Login = Login;