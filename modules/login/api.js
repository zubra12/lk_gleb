var _ = require('lodash');
var async = require('async');
var router   = require('express').Router();
var passport = require(__base+'lib/passport.js');
var mongoose = require("mongoose");
var  bus     = require(__base+'lib/bus.js')


router.get('/me',  function(req,res){
	res.json({me:req.user});
});

router.post('/logout',function(req,res){
	req.session.destroy();
	return res.end();
});


var UserIniter = (new function(){
	var self = this;

	self.logins = [
		"zuch","admin","user"
	];

	self.checkUsers = function(done){
		var uModel = mongoose.model("user");
		uModel.find().lean().exec(function(err,users){
			if (err) return done(err);
			async.each(self.logins,function(login,next){
				if (_.find(users,{LoginUser:login})) return next();
				var u = new uModel({LoginUser:login});
				u.password = "1";
				u.save(next);
			},done)
		})

	}



	return self;
})


bus.on("mongoose-connected",function(){
	UserIniter.checkUsers(function(err){
		if (err) {
			console.log(err);	
		} else {
			console.log("users are checked");
		}
	})
})

router.post('/login', function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		if (info && info.message) {
			return res.json({err:info.message})
		} else {
			if (!user)  return next('user not found');
			req.logIn(user, function(err) {
				return res.json({status:'ok'});
			});
		}
	})(req, res, next);
});

module.exports = router;