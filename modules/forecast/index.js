var Forecast = (new function() {

    var self = new Module("mapslide");

    self.tableData = ko.observableArray();
    self.forecastData = ko.observableArray();

    self.init = function(done) {
        var data = [
            {title:"189 KWh",diff:"- 4%",css:"negative"},
            {title:"23.2 MW",diff:"+ 28%",css:"positive"},
            {title:"23 RECS",diff:"+ 17%",css:"positive"}
        ]
        self.tableData(data);
        return _.isFunction(done) && done();
    }

    return self;
})

ModuleManager.Modules.Forecast = Forecast;


ko.bindingHandlers["forecast"] = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var margin = { top: 0, right: 0, bottom: 0, left: 0 },
            width = 310 - margin.left - margin.right,
            height = 100 - margin.top - margin.bottom;
        var x = d3.scaleTime().range([0, width]);
        var y = d3.scaleLinear().range([height, 0]);
        // append the svg object to the body of the page
        var svg = d3.select(element)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");



        // get the data
        d3.csv("https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/1_OneNum.csv", function(data) {

            // add the x Axis
            var x = d3.scaleLinear()
                .domain([0, 1000])
                .range([0, width]);
            svg.append("g")
                .attr("transform", "translate(0," + height + ")")

            // add the y Axis
            var y = d3.scaleLinear()
                .range([height, 0])
                .domain([0, 0.01]);
    

            // Compute kernel density estimation
            var kde = kernelDensityEstimator(kernelEpanechnikov(7), x.ticks(40))
            var density = kde(data.map(function(d) { return d.price; }))

            // Plot the area
            svg.append("path")
                .attr("class", "mypath")
                .datum(density)
                .attr("fill", "#CFCFCF")
                .attr("opacity", ".8")
                .attr("stroke", "#FFF")
                .attr("stroke-width", 1)
                .attr("stroke-linejoin", "round")
                .attr("d", d3.line()
                    .curve(d3.curveBasis)
                    .x(function(d) { return x(d[0]); })
                    .y(function(d) { return y(d[1]); })
                );

        });

        function make_x_gridlines() {
            return d3.axisBottom(x)
                .ticks(5)
        }

        function make_y_gridlines() {
            return d3.axisLeft(y)
                .ticks(5)
        }

        svg.append("g")
            .attr("class", "grid")
            .call(make_y_gridlines()
                .tickSize(-width)
                .tickFormat("")
            )

        // Function to compute density
        function kernelDensityEstimator(kernel, X) {
            return function(V) {
                return X.map(function(x) {
                    return [x, d3.mean(V, function(v) { return kernel(x - v); })];
                });
            };
        }

        function kernelEpanechnikov(k) {
            return function(v) {
                return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
            };
        }
    }
}