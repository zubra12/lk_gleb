var Footprint = (new function() {

    var self = new Module("footprint");

    self.barsData = ko.observableArray();

    self.tableData = ko.observableArray();
    self.footprintData = ko.observableArray();

    self.init = function(done) {
        self.barsData([
            {title:"Green production",value:"55%", color:"#026093"},
            {title:"Green consumption",value:"83%", color:"#BC3E58"},
            {title:"Grid consumption",value:"35%", color:"#00A9B7"},
            {title:"Grid demand",value:"60%", color:"#E2AD40"}
        ])
        var data = [];
        self.tableData([
            {title:"Percent match",value:"52%",info:""},
            {title:"Carbon offset",value:"524",info:"Tons"},
            {title:"Grid Ussage",value:"451",info:"TWh"},
            {title:"Green Usage",value:"397",info:"TWh"}
        ]);
        return _.isFunction(done) && done();
    }

    return self;
})

ModuleManager.Modules.Footprint = Footprint;


ko.bindingHandlers["footprint"] = {
    init: function(element, valueAccessor, allBindingsAccessor) {

        var margin = { top: 0, right: 0, bottom: 0, left: 0 },
            width = 230 - margin.left - margin.right,
            height = 100 - margin.top - margin.bottom;



        // append the svg object to the body of the page
        var svg = d3.select(element)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");


            var defs = svg.append("defs");
            var gradient = defs.append("linearGradient")
               .attr("id", "svgGradientRedBar")
               .attr("x1", "0%")
               .attr("x2", "0%")
               .attr("y1", "0%")
               .attr("y2", "100%");
            gradient.append("stop")
               .attr('class', 'start')
               .attr("offset", "0%")
               .attr("stop-color", "#FB9377")
               .attr("stop-opacity", 1);
            gradient.append("stop")
               .attr('class', 'end')
               .attr("offset", "100%")
               .attr("stop-color", "#C80C1C")
               .attr("stop-opacity", 1);   
            var gradient2 = defs.append("linearGradient")
               .attr("id", "svgGradientBlueBar")
               .attr("x1", "0%")
               .attr("x2", "0%")
               .attr("y1", "0%")
               .attr("y2", "100%");
            gradient2.append("stop")
               .attr('class', 'start')
               .attr("offset", "0%")
               .attr("stop-color", "#009FBB")
               .attr("stop-opacity", 1);
            gradient2.append("stop")
               .attr('class', 'end')
               .attr("offset", "100%")
               .attr("stop-color", "#2B3D81")
               .attr("stop-opacity", 1);   


        // Parse the Data
        d3.csv("https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_stacked.csv", function(data) {
            console.log(data);

            // List of subgroups = header of the csv files = soil condition here
            var subgroups = data.columns.slice(2)

            // List of groups = species here = value of the first column called group -> I show them on the X axis
            var groups = d3.map(data, function(d) { return (d.group) }).keys()

            // Add X axis
            var x = d3.scaleBand()
                .domain(groups)
                .range([0, width])
                .padding([0.2])
            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x).tickSize(0));

            // Add Y axis
            var y = d3.scaleLinear()
                .domain([0, 40])
                .range([height, 0]);


            // Another scale for subgroup position?
            var xSubgroup = d3.scaleBand()
                .domain(subgroups)
                .range([0, x.bandwidth()])
                .padding([0.05])

            // color palette = one color per subgroup
            var color = d3.scaleOrdinal()
                .domain(subgroups)
                .range(['url(#svgGradientBlueBar)','url(#svgGradientRedBar)'])

           function make_y_gridlines() {
                return d3.axisLeft(y)
                    .ticks(5)
            }
            svg.append("g")
            .attr("class", "grid")
            .call(make_y_gridlines()
                .tickSize(-width)
                .tickFormat("")
            )


            // Show the bars
            svg.append("g")
                .selectAll("g")
                // Enter in data = loop group per group
                .data(data)
                .enter()
                .append("g")
                .attr("transform", function(d) { return "translate(" + (x(d.group)) + ",0)"; })
                .selectAll("rect")
                .data(function(d) { return subgroups.map(function(key) { return { key: key, value: d[key] }; }); })
                .enter().append("rect")
                .attr("x", function(d) { return xSubgroup(d.key); })
                .attr("y", function(d) { return y(d.value); })
                .attr("width", xSubgroup.bandwidth())
                .attr("height", function(d) { return height - y(d.value); })
                .attr("fill", function(d) { return color(d.key); });

        })


    }
}