var Wallet = (new function() {

    var self = new Module("wallet");

    self.tableData = ko.observableArray();

    self.countryData = ko.observableArray();

    self.recsData = ko.observableArray();


    self.init = function(done) {
        var data = [];
        self.tableData([
            { date: "2013-01", value: 58.13 },
            { date: "2013-02", value: 53.98 },
            { date: "2013-03", value: 89.70 },
            { date: "2013-04", value: 99.00 },
        ]);
        self.recsData([
            { title: "Available", status: "positive", value: "524.7 MWh" },
            { title: "Quantity", status: "positive", value: "413" },
            { title: "Consumed", status: "neutral", value: "1.032" },
        ])
        self.countryData([
            { flag: "flag-icon-us", country: "US", value: "89 MWh" },
            { flag: "flag-icon-ca", country: "CA", value: "66 MWh" },
            { flag: "flag-icon-gb", country: "GB", value: "61 MWh" },
            { flag: "flag-icon-fr", country: "FR", value: "16 MWh" }
        ])
        return _.isFunction(done) && done();
    }

    return self;
})

ModuleManager.Modules.Wallet = Wallet;


ko.bindingHandlers["wallet"] = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var margin = { top: 0, right: 0, bottom: 0, left: 0 },
            width = 100 - margin.left - margin.right,
            height = 100 - margin.top - margin.bottom;



        // append the svg object to the body of the page
        var svg = d3.select(element)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");


        // Parse the Data
        d3.csv("https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_stacked.csv", function(data) {
            console.log(data);

            data = [
                {group: "banana",col1: "4",col2: "8",col3: "12",col4: "16"}
            ];

            // List of subgroups = header of the csv files = soil condition here
            var subgroups = ["col1", "col2", "col3", "col4"];

            // List of groups = species here = value of the first column called group -> I show them on the X axis
            var groups = d3.map(data, function(d) { return (d.group) }).keys()

            // Add X axis
            var x = d3.scaleBand()
                .domain(groups)
                .range([0, width])
                .padding([0.1])
            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x).tickSize(0));

            // Add Y axis
            var y = d3.scaleLinear()
                .domain([0, 40])
                .range([height, 0]);


            // Another scale for subgroup position?
            var xSubgroup = d3.scaleBand()
                .domain(subgroups)
                .range([0, x.bandwidth()])
                .padding([0.0])

            // color palette = one color per subgroup
            var color = d3.scaleOrdinal()
                .domain(subgroups)
                .range(['#00527D', '#007B77','#009C74','#01C66C'])

            // Show the bars
            svg.append("g")
                .selectAll("g")
                // Enter in data = loop group per group
                .data(data)
                .enter()
                .append("g")
                .attr("transform", function(d) { return "translate(" + (x(d.group)) + ",0)"; })
                .selectAll("rect")
                .data(function(d) { return subgroups.map(function(key) { return { key: key, value: d[key] }; }); })
                .enter().append("rect")
                .attr("x", function(d) { return xSubgroup(d.key); })
                .attr("y", function(d) { return y(d.value); })
                .attr("width", xSubgroup.bandwidth())
                .attr("height", function(d) { return height - y(d.value); })
                .attr("fill", function(d) { return color(d.key); });

        })
    }
}