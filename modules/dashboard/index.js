var Dashboard = (new function () {

    var self = new Module("dashboard");

    self.templateNames = ko.observableArray();

    self.init = function(done){
		var configs = _.map(ModuleManager.ModulesConfigs,"config");
        var filtered = _.filter(configs,{main_screen:true});
        var mainScreens = _.sortBy(filtered,"sort_index");
        var templates = [];
        mainScreens.forEach(function(cfg){
            templates.push(cfg.id+"-main-screen");
        })
    	self.templateNames(templates);
        return _.isFunction(done) && done();
    }

    return self;
})

ModuleManager.Modules.Dashboard = Dashboard;

