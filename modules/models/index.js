var MModels = (new function () {

    var self = new Module("models");

    self.Models = {};

    self._modelsConfig = {};
    

    self.config = function(){
        return _.cloneDeep(self._modelsConfig);
    }

	self.init = function(done){ 
		self.rGet("config",{},function(cfg){
			self._modelsConfig = cfg;
			self.initModels();
			return done && done();
		})
    }

    self.variants = function(modelName,fieldName){
        var variants = [];
        try{
            variants = self.config()[modelName].fields[fieldName].variants;
        } catch(e){
            console.log(e);
        }
        return variants;
    }

    self.initModels = function(){
        var conf = self.config();
    	for (var modelName in conf){
    		var config = {
    			editFields:conf[modelName].editFields,
    			types:{},
    			default:{},
    			modelName:modelName,
    			editTemplate:{}, 
    			mask:{}
    		};
    		var fields = conf[modelName].fields;
    		for (var fieldName in fields){
    			var info = fields[fieldName];
				config.types[fieldName] = {type:info.type}
                if (info.template){
                    config.editTemplate[fieldName] = info.template;
                } else {
                	var ts = {
                		"Boolean":"form_checkbox",
                		"Number":"form_number",
                		"String":"form_text",
                		"Array":"form_array"
                	};
                	config.editTemplate[fieldName] = ts[info.type];
                }
                if (info.mask) config.mask[fieldName] = info.mask;
                if ( _.has(info, "default")) {
                    config.default[fieldName] = _.clone(info.default);
                } else if (_.isArray(info)){
                    config.default[fieldName] = [];
                }
    		}
            conf[modelName].default = _.clone(config.default);
    		self.Models[modelName]  = function(cfg){
    			return function(){
					var self = this;
					for (var key in cfg){
						self[key] = function(C){
							return C;
						}(cfg[key]);
					}
                    self.template = function(fieldName){
                        return cfg.editTemplate[fieldName] || "form_text";
                    }
                    self.fieldConfig = function(fieldName){
                        return MModels.config()[self.modelName].fields[fieldName];
                    }
                    self.isEdit = ko.observable(false);
					ko.mapping.fromJS(cfg.default, this.mapping || {}, this);
				}
    		}(config);
    	}
    }

    self.create = function(modelName, props) {
        var conf = self.config();
        var default_props = conf[modelName].default;
		if(!modelName || !self.Models[modelName]) return null;
        var inst = new self.Models[modelName](default_props);
        props = _.merge(default_props,props);
        if (props) {
            var mapping = inst.mapping || {};
            if (typeof props === "string") {
                ko.mapping.fromJSON(props, mapping, inst);
            } else {
                ko.mapping.fromJS(props, mapping, inst);
            }
        }
        inst.toJSON = function() {
			return ko.mapping.toJSON(this);
		}
        inst.toJS = function() {
			return ko.mapping.toJS(this);
		}
        inst.fromJS = function(js) { 
        	return ko.mapping.fromJS(js, this.mapping || {}, this);
        }
        inst.fromJSON = function(json) {
			return ko.mapping.fromJSON(json, this.mapping || {}, this);
		}
        return inst;
	}
 
    self.saveFileToGfs = function(file2Save,done){
        if (typeof file2Save!='object') return done(null, null);
        FileAPI.upload({
            url: "/api/gfs",
            files: { file: file2Save },
            complete: function(err, xhr) {
                if (err) return done(err);
                var res = JSON.parse(xhr.response)
                if (!res.id) return done("Ошибка добавления файла");
                return done(null, res.id);
            }
        })
    }

    return self;
})


var ModelTableEdit = (new function() {

    var self = new Module("models");

    self.list = ko.observableArray();
    self.modelsCount = ko.observable(0);

    self.modelName = ko.observable();
    self.search = ko.observable("").extend({
        throttle: 600
    });

    self.isInited = ko.observable(false);

    self.basePath = ko.observable(null);
    self.filter = ko.observable(null);
    self.limit = ko.observable(50);
    self.skip = ko.observable(0);
    self.choosed = ko.observable(null);
    self.loadedModel = ko.observable(null);
    self.pendingRequest = ko.observable(false);

    self.tableFields = ko.observableArray();
    self.links = ko.observableArray();
    self.editFields = ko.observableArray();
    self.readOnlyFields = ko.observableArray();

    self.customfields = ko.observable();

    self.getEditFields = function() {
        return self.editFields();
    }

    self.isChoosed = function(data) {
        return _.isFunction(data._id) && data._id() == self.choosed();
    }

    self.noAccess = ko.observable(true);

    self.sort = ko.observable(null);

    self.setChoosed = function(data) {
        self.choosed(data._id());
    }

    self.validationMessage = ko.observable();

    self.choosed.subscribe(function(v) {
        if (self.fullListView()){
            return;
        } else {
            self.changeChoosedPath();
            if (v) {
                self.loadModel();
            } else {
                self.loadedModel(null);
            }
        }
    })

    self.changeChoosedPath = function() {
        var query = {};
        if (!_.isEmpty(window.location.search)) {
           query = window.location.search.queryObj()
        }
        if (self.choosed()) {
            query.choosed = self.choosed();
        } else {
            delete query.choosed
        }
        var state = window.location.origin + window.location.pathname + toQueryString(query);
        history.replaceState({}, '', state);
    }

    self.add = function() {
        if (self.noAccess()) return;
        self.choosed(null);
        var n = MModels.create(self.modelName(), {});
        self.loadedModel(n);
        Bus.emit("modelcreated");
    }

    self.addByTemplate = function() {
        if (self.noAccess() || _.isEmpty(self.choosed())) return;
        var template = self.loadedModel().toJS();
        delete template["_id"];
        self.choosed(null);
        self.loadedModel(MModels.create(self.modelName(),template));
        Bus.emit("modelcreated");
    }

    self.deleteTable = function(data){
        self.choosed(data._id());
        self.delete();
    }

    self.editTable = function(data){
        self.choosed(data._id());
        self.loadedModel(data);
        $("add_edit_model_modal").modal("open");
        //console.log("edit data",data.toJS());
    }

    self.delete = function() {
        self.confirm("Вы собираетесь удалить ",self.deleteModel);
    }

    self.loadModel = function() {
        self._loadModel(self.choosed(), function() {
            Bus.emit("modelloaded");
        })
    }

    self.reloadModel = function(_id, done) {
        self._loadModel(_id, done);
    }

    self._loadModel = function(_id, done) {
        self.rGet("model",{
                modelName: self.modelName(),
                _id: _id
        },function(data){
            self.loadedModel(MModels.create(self.modelName(), data));
            return _.isFunction(done) && done();
        })
    }

    self.saveFiles = function(done){
         var model = self.loadedModel();
         var fields = self.editFields();
         var toSave = {};
         fields.forEach(function(field){
            var cfg = model.fieldConfig(field);
            if (!_.isEmpty(cfg) && !_.isEmpty(cfg.subtype) && cfg.subtype=="gfsfile"){
                var value = (_.isFunction(model[field]))? model[field]() : null;
                if (_.isObject(value)){
                    toSave[field] = value;    
                }                
            }
         })
         if (_.isEmpty(toSave)) return done();
         var toWork = [];
         for (var fieldName in toSave){
            toWork.push({field:fieldName,file:toSave[fieldName]});
         }
         var replaces = {};
         async.each(toWork,function(workunit,next){
            MModels.saveFileToGfs(workunit.file,function(err,id){
                replaces[workunit.field] = "/api/gfs/"+id;
                return next(err);
            })

         },function(err){
            if (err) return Logger.error(err);
            return done(replaces);
         })
    }


    self.save = function() {
        if (ModelTableEdit.noAccess() || _.isEmpty(ModelTableEdit.loadedModel())) return;
        self.saveFiles(function(replaces){
            if (!_.isEmpty(replaces)){
                for (var fieldName in replaces){
                    self.loadedModel()[fieldName] = replaces[fieldName];
                }
            }
            self.rPut("model",{
                model: self.modelName(),
                _id: self.choosed(),
                data: self.loadedModel().toJS()
            },function(data){
                self.loadList();
                self.choosed(data._id);
                self.reloadModel(data._id, function() {
                    setTimeout(function() {
                        Bus.emit("modelsaved");
                    }, 0);
                })
            })
        })        
    }

    self.deleteModel = function(force) {
        self.rDelete("model",{
            model: self.modelName(),
            _id: self.choosed()
        }, function(){
            self.choosed(null);
            self.loadList();
            self.changeChoosedPath();
            Bus.emit("modeldeleted");
        })
    }

    self.fullListView = ko.observable(false);

    self.subscribtions = {

    };

    self.initModel = function(params) { 
        params.filter = params.filter || {};
        self.clear();
        for (var k in params){            
            if (_.isFunction(self[k])) {
                if (ko.isObservable(params[k])){
                    if (self.subscribtions[k]){
                        self.subscribtions[k].dispose();
                    }
                    self.subscribtions[k] = function(k,kofunc){
                        return kofunc.subscribe(function(value){
                            self[k](value);
                        })
                    }(k,params[k])
                } else {
                    self[k](params[k]);       
                }                
            }
        }
        self.isInited(true);
        self.skip(0);
        self.loadList(function(){
            var query = window.location.search.queryObj();
            var initChoosed = query.choosed;
            if (initChoosed) {
                self.choosed(initChoosed);
            }
        });
        self.noAccess(!Permissions.check(["admin","manager","teamlead"]));
        self.subscribeButtons();
    }

    self.subscribeButtons = function() {
        self.unSubscribeButtons();
        Bus.on("save", self.save)
        Bus.on("addrecord", self.add)
        Bus.on("addrecordtemplate", self.addByTemplate)
    }

    self.unSubscribeButtons = function() {
        Bus.off("save", self.save)
        Bus.off("addrecord", self.add)
        Bus.off("addrecordtemplate", self.addByTemplate)
    }

    self.clear = function() {
        self.modelsCount(0);
        self.isInited(false);
        self.search("");
        self.basePath(null);
        self.fullListView(false);
        self.list([]);
        self.tableFields([]);
        self.editFields([]);
        self.modelName(null);
        self.customfields(null);
        self.choosed(null);
        self.loadedModel(null);
        self.skip(0);
        self.limit(50);
        self.sort(null);
        self.noAccess(true);
        self.filter(null);
        self.validationMessage(null);
        self.unSubscribeButtons();
        if (!_.isEmpty(self.subscribtions)){
            for (var varName in self.subscribtions){
                self.subscribtions[varName].dispose();
            }
        }
    }

    self._loadList = function(done){
        self.rGet("models",{params:JSON.stringify({
            modelName:self.modelName(),
            fields:self.tableFields(),
            sort:self.sort(),
            search:self.search(),
            skip:self.skip(),
            limit:self.limit(),
            filter:self.filter()
        })},done);
    }

    self.loadList = function(done) {
        if (_.isEmpty(self.modelName())) return _.isFunction(done) && done();
        self._loadList(function(data){
            self.list(_.map(data.models,function(modelData){
                return MModels.create(self.modelName(), modelData);
            }));
            self.modelsCount(data.counter);
            return _.isFunction(done) && done();
        })
    }

    self.loadMore = function(done) {
        if (_.isEmpty(self.modelName()) || self.modelsCount() < (self.limit() + self.skip())) {
            self.pendingRequest(false);
            return;
        }
        if (self.pendingRequest()) return;
        self.pendingRequest(true);
        self.skip(self.skip()+self.limit());
        self._loadList(function(data){
            self.list(_.union(self.list(), _.map(data.models, function(modelData) {
                return MModels.create(self.modelName(), modelData);
            })));
            self.pendingRequest(false);
            return _.isFunction(done) && done();
        })      
    }

    self.search.subscribe(function(val) {
        if (self.isInited()) {
            self.skip(0);
            self.loadList();
        }
    })
  

    self.scrolled = function(data, event) {
        var elem = event.target;
        if ((elem.scrollTop > (elem.scrollHeight - elem.offsetHeight - 10)) && (!self.pendingRequest())) {
            self.pendingRequest = true;
            self.loadMore();
        }
    }


    return self;
})





ModuleManager.Modules.Models = MModels;


ko.bindingHandlers.stopBubble = {
  init: function(element) {
    ko.utils.registerEventHandler(element, "click", function(event) {
         event.cancelBubble = true;
         if (event.stopPropagation) {
            event.stopPropagation(); 
         }
    });
  }
};



String.prototype.translit = function(){
    return this.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi,
        function (all, ch, space, words, i) {
            if (space || words) {
                return space ? '-' : '';
            }
            var code = ch.charCodeAt(0),
                index = code == 1025 || code == 1105 ? 0 :
                    code > 1071 ? code - 1071 : code - 1039,
                t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh',
                    'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
                    'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh',
                    'shch', '', 'y', '', 'e', 'yu', 'ya'
                ]; 
            return t[index];
        })
}

