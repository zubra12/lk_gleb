var MapSlide = (new function () {

    var self = new Module("mapslide");
    self.tableData = ko.observableArray();

    self.init = function(done){
		var data = [
			{title:"Clartmont Village Site",value:113},
			{title:"Yucca Valley Site",value:112},
			{title:"Castro Valley Site",value:97},
			{title:"Hesperia Site",value:78},
			{title:"Fourth St Site",value:75},
		]
		data.forEach(function(d){
			d.height = (d.value-50)*1+"px";
		})
		self.tableData(data);
        return _.isFunction(done) && done();
    }

    return self;
})

ModuleManager.Modules.MapSlide = MapSlide;


ko.bindingHandlers["map"] = {
    init: function(element, valueAccessor, allBindingsAccessor) {
		var svg = d3.select(element),
		    width = +svg.attr("width"),
		    height = +svg.attr("height");

		// Map and projection
		var projection = d3.geoNaturalEarth()
		    .scale(width / 2 / Math.PI)
		    .translate([width / 3.4, height / 1.5])

		// Load external data and boot
		d3.json("https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/world.geojson", function(data){

		    // Draw the map
		    svg.append("g")
		        .selectAll("path")
		        .data(data.features)
		        .enter().append("path")
		            .attr("fill", "#FFF")
		            .attr("d", d3.geoPath()
		                .projection(projection)
		            )
		            .style("stroke", "#fff")
		})
    }
}



