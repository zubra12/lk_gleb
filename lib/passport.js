var passport = require('passport');
var LocalStrategy  = require('passport-local').Strategy;
var mongoose = require('mongoose');
var Bus = require("./bus.js");

passport.use(new LocalStrategy(
  function(username, password, done) {
    username = (username+'').toLowerCase().trim(); 

    console.log({$or:[{LoginUser: username},{Mail: username}]});

    mongoose.model('user').findOne({$or:[{LoginUser: username},{Mail: username}]}, 'LoginUser PassHash PassSalt').exec(function (err, user) {
      if (err) { 
        Bus.emit('localauth',{status:"error",message:err,username:username});
        return done(err); 
      }
      if (!user) {
        Bus.emit('localauth',{status:"error",message:"wrongusername",username:username});
        return done(null, false, { message: 'wrong user name' });
      }
      if (!user.checkPassword(password)) {
        Bus.emit('localauth',{status:"error",message:"wrongpassword",username:username});
        return done(null, user._id, { message: 'wrong password' });
      }
      Bus.emit('localauth',{status:"success",user:user._id,username:username});
      return done(null, user);
    });
  }
));

passport.serializeUser(function(user, done) {
	done(null, user._id);
});

passport.deserializeUser(function(id, done) {
	mongoose.model('user').findById(id, function(err, user) {
		done(err, user);
	});
});


module.exports = passport;