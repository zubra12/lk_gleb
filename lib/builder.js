var _ = require("lodash");
var async = require("async");
var fs = require("fs");
var pluginmanager = require('./pluginmanager.js');


var Compiller = (new function(){
    
    var self = this;

    self.Cache = {};
    var JSMIN = {}; JSMIN[__base+'static/build/index.modules.js'] = [__base+'static/build/index.modules.max.js'];

    self.buildBundle = function(final){
        var gr_config = {
            pkg: {name:'build_modules'},
            concat:{
                "modules_js":{
                     src:[],
                     dest:__base+'static/build/index.modules.max.js'
                },
                "modules_html":{
                     src:[],
                     dest:__base+'static/build/template.html'
                },
                "modules_css":{
                     src:[],
                     dest:__base+'static/build/index.max.css'
                }
            },
            cssmin:{
                "modules_css_min":{
                    src:  __base+'static/build/index.modules.max.css',
                    dest: __base+'static/build/index.modules.css'
                }
            },
            uglify:{
                "modules_js_min":{
                    files: JSMIN
                }
            }

        };
        var _remap = function(list){
            return _.map(list,function(l){
                return __base+l.substring(1);
            })
        }
        async.each(["index.js","index.css","template.html"],function(fileType,done){
            self.files(fileType,function(err,JSFilesList){
                var dest = null;
                switch(fileType){
                    case "index.js":
                        gr_config.concat.modules_js.src = _remap(JSFilesList);
                    break;
                    case "index.css":
                        gr_config.concat.modules_css.src = _remap(JSFilesList);
                    break;
                    case "template.html":
                        gr_config.concat.modules_html.src = _remap(JSFilesList);
                    break;
                }
                return done();
            })
        },function(err){
            var grunt = require('grunt');
            grunt.task.init = function() {};
            grunt.initConfig(gr_config);
            grunt.loadNpmTasks('grunt-contrib-concat');
            grunt.loadNpmTasks('grunt-css');
            grunt.loadNpmTasks('grunt-contrib-uglify');
            grunt.loadNpmTasks('grunt-contrib-cssmin');
            grunt.tasks(['concat','cssmin','uglify'], {}, function() {
                fs.writeFileSync(__base+"static/build/index.html",[
                    '<link rel="stylesheet" href="/build/index.modules.css">',
                    '<script src="/build/index.modules.js" ></script>',
                    '<span src="/build/template.html" ></span> '
                ].join("\n\n"));
                grunt.log.ok('Done running tasks.');
                console.log("Rebuild bundle is done !");
                return _.isFunction(final) && final();
            });
        })
    }


    self.build = function(done){
        var Content = {};
        async.each(["Css","Js","Templates"],function(Type,cb){
            self[Type](function(err,Cont){
                Content[Type] = Cont;
                return cb();
            })
        },function(err){
            fs.writeFileSync(__base+"static/build/index.html",[Content.Css,Content.Js,Content.Templates].join("\n\n"));
            console.log("BUILD is done");
            done && done();
        })

    }

    self.enabledMods = function(done){
        pluginmanager.enabledExtensions(function(err,modules){
            self.Cache["Modules"] = modules;
            return done(null,self.Cache["Modules"])
        })
    };


    self.Css = function(done){
        self.files("index.css",function(err,FilesList){
            var Content = [];
            FilesList.forEach(function(F){
                Content.push('<link rel="stylesheet" href="'+F+'"> ');
            })
            return done(null,Content.join("\n"));
        })
    }

    self.Js = function(done){
        self.files("index.js",function(err,FilesList){
            var Content = [];
            FilesList.forEach(function(F){
                Content.push('<script src="'+F+'" ></script>');
            })
            return done(null,Content.join("\n"));
        })
    }

    self.Templates = function(done){
        self.files("template.html",function(err,FilesList){
           var Content = [];
            FilesList.forEach(function(F){
                Content.push('<span src="'+F+'" ></span>');
            })
            return done(null,Content.join("\n"));
        })
    }

    self.files = function(mask,done){
        self.enabledMods(function(err,list){
            var result = [], fs = require("fs");
            async.each(list,function(moduleName,cb){
                fs.stat(__base+'modules/'+moduleName+'/'+mask,function(err,stat){
                    if (stat && stat.size){
                        result.push("/modules/"+moduleName+'/'+mask);
                    }
                    return cb();
                })
            },function(err){
                return done(err,result)
            });
        })
    }


    return self;
})


module.exports = Compiller;