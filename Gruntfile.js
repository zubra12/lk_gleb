module.exports = function(grunt) {
    grunt.initConfig({
        pkg: {
            name: 'tochka_lib'
        },
        concat: {
            lib_js: {
                src: [
                    "static/lib/js/EventEmitter.min.js",
                    "static/lib/js/async.js",
                    "static/lib/js/FileAPI.min.js",
                    "static/lib/js/jquery-2.1.4.js",                    
                    "static/lib/js/jquery.history.js",
                    "static/lib/js/jquery.include-min.js",
                    "static/lib/js/knockout-3.4.2.js",
                    "static/lib/js/knockout.mapping-latest.debug.js",                    
                    "static/lib/js/kopersist.js",
                    "static/lib/js/lodash.min.js",
                    "static/lib/js/moment.min.js",
                    "static/lib/js/moment-ru.js",
                    "static/lib/js/moment-range.js",
                    "static/lib/js/pager.js",
                    "static/lib/js/sweetalert2.all.min.js",
                    "static/lib/materialize/materialize.js"                    
                ],
                dest: 'static/build/lib.js'
            },
            lib_css: {
                src: [
                    "static/lib/css/font-awesome.min.css",
                    "static/lib/css/font-awesome-animation.min.css",
                    "static/lib/css/pager.css",
                    "static/lib/css/flaticon.css",
                    "static/lib/css/flag-icon.min.css",
                    "static/lib/materialize/materialize.css"
                ],
                dest: 'static/build/lib.css'
            }
        },
        cssmin: {
            lib_css: {
                src: 'static/build/lib.css',
                dest: 'static/build/lib.min.css'
            }
        },
        uglify: {
            lib_js: {
                files: {
                    'static/build/lib.min.js': ['static/build/lib.js'],
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-css');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-shell');
    grunt.registerTask('default', ['concat', 'cssmin', 'uglify']);
};
