const config      = require('./config.js')
    , express        = require('express')
    , app            = express()
    , mongoose       = require('mongoose')
    , sessions       = require('express-session')
    , MongoStore     = require('connect-mongo')(sessions)
    , sessionStore   = new MongoStore(config.mongoSessionsDB)
    , E_PORT         = process.env.PORT || config.port
    , bodyParser     = require('body-parser')
    , bus     = require('./lib/bus.js')
    , cookieParser   = require('cookie-parser')
    , queryString = require('querystring')
    , _ = require('lodash')
    , Builder = require('./lib/builder.js')


mongoose.Promise = global.Promise;

/*
 * Workaround for mongoose throwing an exception on initial connection fail
 * */
const { Server } = require('mongodb-core');
const { URL } = require('url');
const urlObj = new URL(config.mongoUrl);
const server = new Server({
    hostname: urlObj.hostname,
    port: urlObj.port || 27017,
    reconnect: true,
    reconnectInterval: 8192
});
server.on('connect', () => {
    mongoose.connect(config.mongoUrl, {
        promiseLibrary: Promise,
        useNewUrlParser: true,
        connectTimeoutMS: 600000,
    });
    setTimeout(server.destroy.bind(server), 4096);
});
server.connect();

mongoose.connection.on('connected', function(){
    const ModelInit = require('./lib/initdb.js');
    ModelInit(function(){
        app.set('port',E_PORT);
        const CookieConfig = config.cookieConfig;
        CookieConfig.store = sessionStore;
        app.use(cookieParser(CookieConfig.secret));
        app.use(sessions(CookieConfig));
        app.use(function(req, res, next) {
            var base = req.originalUrl.split("?")[0];
            req.base = base;
            console.log(req.base);
            next();
        });

        const passport = require('./lib/passport.js');
        app.use(passport.initialize());
        app.use(passport.session());
        app.use(bodyParser.json({limit: '500mb',parameterLimit: 10000}));
        app.use(bodyParser.urlencoded({extended: true,limit: '500mb',parameterLimit: 10000}));

        const Plugins = require(__base+'lib/plugins.js');     
        Plugins.Router(function(err,modulesRouter){
            app.use(modulesRouter);
            app.use(function (err, req, res, next) {
                if (_.isObject(err) && err.module){
                    return res.redirect("/error?"+queryString.stringify(err));
                }
                return res.json({err:err+''});
            });                 
            Builder.build(); 
            const environment = process.env.NODE_ENV || "development";
            const http = require("http");
            const server = http.createServer(app);
            server.on('clientError', function(err) {
                console.log('CLIENT ERROR', err);
            });
            if (config.errorCatch) process.on('uncaughtException', function (err,info) {  console.log(err,info); });
            console.log("Starting " + environment + " server on "+E_PORT);
            if (server) server.listen(E_PORT);
            bus.emit("mongoose-connected");
        });
        
    });
})

