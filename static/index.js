var Logger = (new function(){
    var self = this;

    self.error = function(message){
        self.show(message,"error");
        M.toast({html: message});
    }

    self.info = function(message){
        self.show(message,"info");
    }

    self.log = function(message){
        self.show(message,"log");
    }

    self.colors = {
        error:'maroon',
        info:'navy',
        log:'#808080'
    }

    self.show = function(message,type){
        if (!window.console || !window.console.log) return;
        if (!_.isString(message)){
            try{
                message = JSON.stringify(message);      
            } catch(e){
                console.log(message);
            }   
        } 
        console.log('%c >>> '+message, 'color: '+self.colors[type]);
    }

    return self;
})

String.prototype.queryObj = function(){
    return (this).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = decodeURIComponent(n[1]),this}.bind({}))[0];  
}

var toQueryString = function(obj){
    return '?'+decodeURIComponent( $.param(obj) );
}


var Helper = (new function(){

    var self = this;

    self.days = {0:'пн',1:'вт',2:'ср',3:'чт',4:'пт',5:'сб',6:'вс'};

    self.daysOfWeek = [];

    for (var ind in self.days){
        self.daysOfWeek.push({id:ind,name:self.days[ind]});   
    }

    return self;
})

var Bus = (new function(){

    var self = this;

    self.events = new EventEmitter();

    self.on = function(){
        self.events.on.apply(self.events, arguments);
    }

    self.off = function(){
        self.events.off.apply(self.events, arguments);
    }
    
    self.emit = function(){
        self.events.emit.apply(self.events, arguments);
    }

    return self;
}) 

ko.extenders.persistUrl = function(target, keyOrObj) {
    var initialValue = target();
    var currentParams = window.location.search.queryObj();
    var initialValue = null;
    var key = "", defaultValue = null;
    if (!_.isObject(keyOrObj)){
        key = keyOrObj;
    } else {
        key = keyOrObj.key;
        defaultValue = keyOrObj.defaultValue;
    }

    if (key && _.has(currentParams,key)) {
        initialValue = currentParams[key];
    }
    if (!initialValue && defaultValue){
        initialValue = defaultValue;
        try{
            
            if (!_.isEmpty(localStorage.getItem(key)) && localStorage.getItem(key)!='undefined'){
                initialValue = JSON.parse(localStorage.getItem(key));
            }
        } catch(e){
            ;   
        }
    } 
    target(initialValue);
    var updateUrl = function(key,value){
        return function(){
            var query = {};
            if (!_.isEmpty(window.location.search)) {
               query = window.location.search.queryObj()
            }
            if (!_.isEmpty(value())){
                query[key] = value();
            } else if (_.has(query,key)){
                delete query[key];
            }
            var state = window.location.origin + window.location.pathname + toQueryString(query);
            console.log("state>>>",state,window.location.origin , window.location.pathname );
            history.replaceState({}, '', state);
        }
    }

    Bus.off("initialnavigate",updateUrl(key,target));
    Bus.off("navigate",updateUrl(key,target));
    Bus.on("navigate",updateUrl(key,target));
    Bus.on("initialnavigate",updateUrl(key,target));

    target.subscribe(function (newValue) {
        var query = {};
        if (!_.isEmpty(window.location.search)) {
           query = window.location.search.queryObj()
        }
        localStorage.setItem(key,JSON.stringify(newValue));
        if (!_.isEmpty(newValue)){
            query[key] = newValue;
        } else if (_.has(query,key)){
            delete query[key];
        }
        var state = window.location.origin + window.location.pathname + toQueryString(query);
        console.log("state>>>",state,window.location.origin , window.location.pathname );
        history.replaceState({}, '', state);
    });
    return target;
};


var MSite = (new function () {

    var self = this;

    self.flashButton = function (icon) {
        var marks = $("i." + icon + ":visible").parent();
        marks.addClass("marked");
        setTimeout(function () {
            marks.removeClass("marked");
        }, 500)
    }

    self.isMobile = ko.observable(false);

    self.checkIsMobile = function(){
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        self.isMobile(check);
    }

    self.noReload = ko.observable(true);

    self.init = function (done) {
        ModuleManager.load(function () {
            ModuleManager.init(function () {
                Bus.emit("initauth");
                return done && done();
            })
        })
    }

    self.Me = ko.observable(null);

    self.subscribe = function(config){
        self.unSubscribe(config);
        console.log("subscribe",config);
        for (var evName in config){
            Bus.on(evName, config[evName]);
        }
    }

    self.unSubscribe = function(config){
        console.log("unsubscribe",config);
        for (var evName in config){
            Bus.off(evName, config[evName]);
        }
    }

    self.announceTimeout = {
        save: null,
        open:null,
        "scroll-bottom": null,
        addrecord: null,
        addrecordtemplate: null,
        refresh: null
    };

    self.announce = function(type,icon,flag){

        self.flashButton(icon);
        if (self.announceTimeout[type]) clearTimeout(self.announceTimeout[type]);
        self.announceTimeout[type] = setTimeout(function () {
            console.log("announce",type,icon,flag);
            Bus.emit(type, flag);
            self.announceTimeout[type] = null;
        }, 500);
    }

    self.announceRefresh = function (noCache) {
        self.announce("refresh","fa-refresh",noCache);
    }

    self.announceNew = function () {
        self.announce("addrecord","fa-file-o",false);
    }

    self.announceNewTemplate = function () {
        self.announce("addrecordtemplate","fa-file-text-o",false);
    }

    self.announceCtrS = function () {
        self.announce("save","fa-save",false);
    }

    self.announceCtrO = function () {
        self.announce("open","fa-folder-open-o",false);
    }

    self.announceScrollBottom = function () {
        if (ModuleManager.isLoading()) return;
        self.announce("scroll-bottom","fa-angle-double-up",false);
    }

    self.start = function () {
        ko.options.deferUpdates = true;
        self.init(function () {
            Bus.emit("inited");
            pager.useHTML5history = true;
            pager.Href5.history = History;
            pager.extendWithPage(self);
            ko.applyBindings(self, $('html')[0]);
            pager.startHistoryJs();
            pager.onBindingError.add(function (event) {
                Logger.error(event);
            })
            Bus.emit("initialnavigate", window.location.search ? window.location.search.queryObj() : {});
            $("body").removeClass("loading");
            document.addEventListener("keydown", function (e) {
                if (e.keyCode == 79 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                    e.preventDefault();
                    self.announceCtrO();                    
                }
                if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                    e.preventDefault();
                    self.announceCtrS();
                }
                if (e.keyCode == 120) {
                    e.preventDefault();
                    self.announceRefresh((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey));
                }
                if (e.keyCode == 45 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                    if (e.shiftKey) {
                        e.preventDefault();
                        self.announceNewTemplate();
                    } else {
                        e.preventDefault();
                        self.announceNew();
                    }
                }
            }, false);
            $(window).scroll(function () {
                if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                    self.announceScrollBottom();
                }
            });
            moment.locale('ru');
            self.checkIsMobile();
        });
        History.Adapter.bind(window, 'statechange', function () {
            Bus.emit("navigate");
            window.scrollTo(0, 0);
        });
        window.onbeforeunload = function () {
            Bus.emit("unload");
        };
    }
})


$(document).includeReady(function () {
    MSite.start();
});
